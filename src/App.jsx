import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchProducts } from "./redux/actions/productsActions";
import { openModal, closeModal } from './redux/actions/productsActions';
import "./App.css";
import CartPage from "./pages/CartPage/CartPage";
import HomePage from "./pages/HomePage/HomePage";
import { Routes, Route, Link } from "react-router-dom";
import Header from "./components/Header/Header";
// import axios from "axios";
import FavoritesPage from "./pages/FavoritesPage/FavoritesPage";

function App() {
  const [activeStarsCount, setActiveStarsCount] = useState(0);
  const [activeArticle, setActiveArticle] = useState(null);
  const [isAdded, setIsAdded] = useState( false);
  const handleCartClick = (productArticle) => {
    // Збереження стану обраного товару в localStorage
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];

    if (!addedToCart.includes(activeArticle)) {
      localStorage.setItem(
        "addedToCart",
        JSON.stringify([...addedToCart, productArticle])
      );
    } else {
      localStorage.setItem(
        "addedToCart",
        JSON.stringify(
          addedToCart.filter((article) => article !== productArticle)
        )
      );
    }
  };

  useEffect(() => {
    // Отримання обраних товарів з localStorage
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setActiveStarsCount(favorites.length);
  }, []);



  /////////////////модалка///////////////////////////////
  const isModalOpen = useSelector(state => state.modal.isModalOpen);

  const handleOpenModal = () => {
    dispatch(openModal());
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };
 /////////////////Продактс///////////////////////////////
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  const { products, loading, error } = useSelector(
    (state) => state.product || { products: [], loading: true, error: null }
  );

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

  return (
    <>
      <Header 
      isAdded={isAdded}
       activeStarsCount={activeStarsCount} 
      />

      <Routes>
        <Route
          path="/"
          element={
            <HomePage
              products={products}
              setIsAdded={setIsAdded}
              setActiveStarsCount={setActiveStarsCount}
              isModalOpen={isModalOpen}
              openModal={handleOpenModal}
              closeModal={handleCloseModal}
              activeArticle={activeArticle}
              setActiveArticle={setActiveArticle}
              handleCartClick={handleCartClick}
            />
          }
        />
        <Route
          path="/сart"
          element={
            <CartPage
              products={products}
              isModalOpen={isModalOpen}
              openModal={handleOpenModal}
              closeModal={handleCloseModal}
              activeArticle={activeArticle}
              setActiveArticle={setActiveArticle}
              handleCartClick={handleCartClick}
              setIsAdded={setIsAdded}
            />
          }
        />
        <Route
          path="/favorites"
          element={
            <FavoritesPage
              products={products}
              setActiveStarsCount={setActiveStarsCount}
            />
          }
        />
      </Routes>
    </>
  );
}

export default App;

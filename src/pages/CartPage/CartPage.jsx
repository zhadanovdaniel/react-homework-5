import Card from "../../components/Card/Card";
import React, { useState, useEffect } from "react";
import Modal from "../../components/Modal/Modal";
import ModalWrapper from "../../components/Modal/ModalWrapper";
import ModalHeader from "../../components/Modal/ModalHeader";
import ModalFooter from "../../components/Modal/ModalFooter";
import ModalBody from "../../components/Modal/ModalBody";
import ModalClose from "../../components/Modal/ModalClose";
import CartForm from "../../components/CartForm/CartForm";

export default function CartPage(props) {
   const { products,isModalOpen,openModal,closeModal ,activeArticle, setActiveArticle,handleCartClick,setIsAdded} = props
  const [addedToCartCards, setAddedToCartCards] = useState([]);
  useEffect(() => {
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setAddedToCartCards(addedToCart);
  }, [addedToCartCards]);

  return (
    <>
      <h1>Твій кошик з футболками</h1>
      {addedToCartCards.length === 0 ? (
        <h2>Поки пусто...</h2>
      ) : (
        <div className="cardWrapper">
          <CartForm 
          setIsAdded={setIsAdded}
           activeArticle={activeArticle}></CartForm>
          {products
            .filter((item) => addedToCartCards.includes(item.article))
            .map((item, index) => (
              <Card
                key={index}
                src={item.image}
                alt={item.name}
                productName={item.name}
                productPrice={item.price}
                productArticle={item.article}
                productColor={item.color}
                // setActiveStarsCount={setActiveStarsCount}
                setActiveArticle = {setActiveArticle}
                ModalOpen={openModal}
                activeCart={true}
                cartTittle={"Remove from cart"}
              />
            ))}
        </div>
      )}

      <Modal isOpen={isModalOpen}>
        <ModalWrapper>
          <ModalHeader>
            <ModalClose onClick={closeModal} />
          </ModalHeader>

          <ModalBody>
            <h1>REMOVE FROM CART</h1>
            <p>
              By clicking the “YES, REMOVE FROM CART” button, product will be removed{" "}
              <br />
              to cart
            </p>
          </ModalBody>
          <ModalFooter
            firstText={"NO, CANCEL"}
            secondaryText={"YES, REMOVE FROM CART"}
            firstClick={closeModal}
            secondaryClick={() => {
              handleCartClick(activeArticle);
              setIsAdded((prev) => !prev);
              closeModal();
            }}
          ></ModalFooter>
        </ModalWrapper>
      </Modal>
    </>
  );
}

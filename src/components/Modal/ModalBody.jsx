export default function ModalBody({ children }) {
    
     return (
    <div className="ModalBody">
      {children}
    </div>
    
    )
  }
  
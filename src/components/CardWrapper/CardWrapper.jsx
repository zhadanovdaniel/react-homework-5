// import React, { useState, useEffect } from "react";
// import Card from "../Card/Card";
// import "./CardWrapper.scss";


// import Modal from "../Modal/Modal";
// import ModalWrapper from "../Modal/ModalWrapper";
// import ModalHeader from "../Modal/ModalHeader";
// import ModalFooter from "../Modal/ModalFooter";
// import ModalBody from "../Modal/ModalBody";
// import ModalClose from "../Modal/ModalClose";


// export default function CardWrapper({ products, setIsAdded, setActiveStarsCount,isModalOpen,openModal,closeModal,activeArticle, setActiveArticle,handleCartClick}) {
  
  
//   useEffect(() => {
//     // Отримання обраних товарів з localStorage
//     const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
//     setActiveStarsCount(favorites.length);
//   }, []);


//      const [activeCartCount, setActiveCartCount] = useState(0);
//      useEffect(() => {
//       // Отримання обраних товарів з localStorage
//       const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
//       setActiveCartCount(addedToCart.length);
//     }, []);
  
//   return (
//     <>
//       <h2>Футболки у асортименті:</h2>
//       <div className="cardWrapper">
//         {products.map((product) => (
//           <Card
//             key={product.article}
//             src={product.image}
//             alt={product.name}
//             productName={product.name}
//             productPrice={product.price}
//             productArticle={product.article}
//             productColor={product.color}
//             ModalOpen={openModal}
//             setActiveStarsCount={setActiveStarsCount} // Передача функції в компонент Card
//             setActiveCartCount={setActiveCartCount}
//             setActiveArticle = {setActiveArticle}
//             activeCart={true}
//             activeStar={true}
//             cartTittle={"Add to cart"}
//           />
//         ))}
//       </div>
      





//       <Modal isOpen={isModalOpen}>
//         <ModalWrapper>
//           <ModalHeader>
//             <ModalClose onClick={closeModal} />
            
//           </ModalHeader>

//           <ModalBody>
//             <h1>Add to cart</h1>
//             <p>
//               By clicking the “YES, ADD TO CART” button, product will be added <br />
//               to cart
//             </p>
//           </ModalBody>
//           <ModalFooter
//             firstText={ "NO, CANCEL"  }
//             secondaryText={ "YES, ADD TO CART"}
           
//             firstClick={closeModal}
//             secondaryClick={()=>{handleCartClick(activeArticle); setIsAdded((prev)=>!prev); closeModal()}}
//           ></ModalFooter>
//         </ModalWrapper>
//       </Modal>
//     </>
//   );
// }
import React, { useState, useEffect } from "react";
import Card from "../Card/Card";
import "./CardWrapper.scss";

import Modal from "../Modal/Modal";
import ModalWrapper from "../Modal/ModalWrapper";
import ModalHeader from "../Modal/ModalHeader";
import ModalFooter from "../Modal/ModalFooter";
import ModalBody from "../Modal/ModalBody";
import ModalClose from "../Modal/ModalClose";

export default function CardWrapper({ products, setIsAdded, setActiveStarsCount, isModalOpen, openModal, closeModal, activeArticle, setActiveArticle, handleCartClick }) {
  
  const [isAddedToCart, setIsAddedToCart] = useState(false);

  useEffect(() => {
    const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
    setActiveStarsCount(favorites.length);
  }, []);

  const [activeCartCount, setActiveCartCount] = useState(0);
  useEffect(() => {
    const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setActiveCartCount(addedToCart.length);
  }, []);

  return (
    <>
      <h2>Футболки у асортименті:</h2>
      <div className="cardWrapper">
        {products.map((product) => (
          <Card
            key={product.article}
            src={product.image}
            alt={product.name}
            productName={product.name}
            productPrice={product.price}
            productArticle={product.article}
            productColor={product.color}
            ModalOpen={openModal}
            setActiveStarsCount={setActiveStarsCount}
            setActiveCartCount={setActiveCartCount}
            setActiveArticle={setActiveArticle}
            activeCart={true}
            activeStar={true}
            cartTittle={isAddedToCart ? "Added to cart" : "Add to cart"} // Здесь меняем текст кнопки в зависимости от состояния
          />
        ))}
      </div>

      <Modal isOpen={isModalOpen}>
        <ModalWrapper>
          <ModalHeader>
            <ModalClose onClick={closeModal} />
          </ModalHeader>

          <ModalBody>
            <h1>Add to cart</h1>
            <p>
              By clicking the “YES, ADD TO CART” button, product will be added <br />
              to cart
            </p>
          </ModalBody>
          <ModalFooter
            firstText={"NO, CANCEL"}
            secondaryText={"YES, ADD TO CART"}
            firstClick={closeModal}
            secondaryClick={() => {
              handleCartClick(activeArticle);
              setIsAdded((prev) => !prev);
              setIsAddedToCart(true); // Устанавливаем состояние добавления в корзину
              closeModal();
            }}
          />
        </ModalWrapper>
      </Modal>
    </>
  );
}

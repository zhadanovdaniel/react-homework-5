import React, { useState, useEffect } from "react";
import "./Header.scss";
import { Link } from "react-router-dom";
export default function Header({ isAdded, activeStarsCount}) {
  const [cartCount, setCartCount] = useState(0);

  useEffect(() => {
    // Отримання кількості товарів у кошику з localStorage
    const cartItems = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCartCount(cartItems.length);
  }, []);

  useEffect(() => {
    // Отримання кількості товарів у кошику з localStorage
    const cartItems = JSON.parse(localStorage.getItem("addedToCart")) || [];
    setCartCount(cartItems.length);

    
  }, [isAdded, !isAdded]);

  return (
    <>
      <header>
        <nav>
          <div className="logo">
            <Link to="/"> Магазин футболок</Link>
          </div>
          <ul className="menu">
            <li>
              <Link to="/">Головна</Link>
            </li>
            <li>
              <a href="#">Галерея</a>
            </li>
            <li>
              <a href="#">Контакти</a>
            </li>
          </ul>
          <div className="icons">
            <a href="#">
              <Link to="/сart">
                <img
                  src="https://www.pngall.com/wp-content/uploads/7/Basket-PNG-Free-Download.png"
                  alt="Кошик"
                />
              </Link>

              <span>{cartCount}</span>
            </a>
            <a href="#">
              <Link to="/favorites">
                <img
                  src="src/assets/favo.png"
                  alt="Обране"
                />
              </Link>

              <span>{activeStarsCount}</span>
            </a>
          </div>
        </nav>
      </header>
    </>
  );
}

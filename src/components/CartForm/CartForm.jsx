import "./CartForm.scss"
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';

export default function CartForm({setIsAdded, activeArticle={activeArticle}}){
  
  const validationSchema = Yup.object().shape({
    firstName: Yup.string().required('Firstname is required'),
    lastName: Yup.string().required('LastName is required'),
    email: Yup.string().email('Invalid email').required('Email is required'),
    year: Yup.number().required('Age is required'),
    address: Yup.string().required('Address is required'),
    phoneNumber: Yup.number().required('PhoneNumber is required')

  })
  
  
  
    const initialValues ={
    firstName:'',
    lastName: '',
    email:'',    
    year:'',
    address:'',
    phoneNumber:'',
    }
    function onSubmit(values){
        // console.log('submit', values);
        // localStorage.removeItem("addedToCart");
        const addedToCart = JSON.parse(localStorage.getItem("addedToCart")) || [];
        // if (!addedToCart.includes(activeArticle)) {
        //     localStorage.setItem(
        //         "addedToCart",
        //         JSON.stringify(
        //           addedToCart.filter((article) => article !== productArticle)
        //         )
        //       );
        //   } 
        //   addedToCart.forEach(article=> {
        //     article !== productArticle
        //  })
        // Установка isAdded в false для всех товаров в корзине
        
        // Очистка localStorage
        localStorage.removeItem("addedToCart");
        setIsAdded((prev) => !prev)
        
    }

    return(
       
        <Formik 
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
        >
            <Form> 
                <div className="form"> 
                    <h2>Платіжні Реквізити:</h2>
                <div className="form_row">
                    <label htmlFor="firstName">Ім'я</label>
                     <Field type="text" id ='firstName' name ="firstName"/>
                     <ErrorMessage name='firstName' component="div"></ErrorMessage>
                </div>
                <div className="form_row">
                    <label htmlFor="lastName">Прізвище</label>
                     <Field type="text" id ='lastName' name ="lastName"/>
                     <ErrorMessage name='lastName' component="div"></ErrorMessage>
                </div>
                <div className="form_row">
                    <label htmlFor="email">Email</label>
                     <Field type="email" id ='email' name ="email"/>
                     <ErrorMessage name='email' component="div"></ErrorMessage>
                </div>
                <div className="form_row">
                    <label htmlFor="year">Вік</label>
                    

                     <Field as= "select" id ='year' name ="year">
                        {
                           new Array(100)
                           .fill('')
                           .map((item,index)=> index+ 1 )
                           .map(year =>(
                            <option value ={year} key={year}>
                                {year}
                            </option>
                           ))
                        }
                     </Field>
                     <ErrorMessage name='year' component="div"></ErrorMessage>
                </div>
                <div className="form_row">
                    <label htmlFor="address">Адреса</label>
                     <Field type="address" id ='address' name ="address"/>
                     <ErrorMessage name='address' component="div"></ErrorMessage>
                </div>
                <div className="form_row">
                    <label htmlFor="phoneNumber">Телефон</label>
                     <Field type="phoneNumber" id ='phoneNumber' name ="phoneNumber"/>
                     <ErrorMessage name='phoneNumber' component="div"></ErrorMessage>
                </div>
                <button type="submit">Замовити</button>
                </div>
            </Form>
        </Formik>
        
    )


}